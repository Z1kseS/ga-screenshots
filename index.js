const Nightmare = require("nightmare");
const screenshotSelector = require("nightmare-screenshot-selector");
const fs = require("fs");
const path = require("path");

require("nightmare-upload")(Nightmare);

Nightmare.action("screenshotSelector", screenshotSelector);

const nightmare = Nightmare({ show: true });

const folder = process.argv[2];
const files = fs.readdirSync(folder);

(async () => {
  await nightmare.viewport(1000, 700).goto("http://localhost:3000/charts");

  for (const file of files) {
    const lowerCaseName = file.toLocaleLowerCase();
    const evaluations = [
      { name: "deba1", value: "deba1" },
      { name: "deba2", value: "deba2" },
      { name: "deba3", value: "deba3" },
      { name: "deba4", value: "deba4" },
      { name: "f15", value: "deba1" },
      { name: "f16", value: "deba2" },
      { name: "f18", value: "deba3" },
      { name: "f19", value: "deba4" }
    ];
    const evalution = evaluations.find(({ name }) =>
      lowerCaseName.includes(name)
    );

    console.log("Evaluation: " + evalution.name);

    if (evalution) {
      await nightmare
        .goto("http://localhost:3000/charts?test=" + file)
        .wait(1000)
        .click("." + "ant-select-selection__rendered")
        .wait(200)
        .click("#" + evalution.value)
        .wait(1000)
        .upload("#fileInput", path.resolve(folder, file))
        .scrollTo(400, 0)
        .wait(1000)
        .screenshotSelector("#chart")
        .then(data => fs.writeFileSync(`./results/${file}.png`, data));
    }
  }

  await nightmare.end();
})();
